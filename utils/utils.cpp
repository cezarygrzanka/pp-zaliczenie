#include "utils.h"

#include <algorithm>
#include <iostream>
#include <math.h>
#include <time.h>

// Declare all functions
int partition(int arr[], int first, int last);
int medianOfThree(int a, int b, int c);
bool isSorted(int arr[], int begin, int end);
void introSortExecute(int arr[], int first, int last, int depth);
void introSort(int arr[], int size);
void insertionSort(int arr[], int first, int last);
void swap(int *a, int *b);
void heapSort(int arr[], int begin, int end);
void swap(int *a, int *b);

void introSortExecute(int arr[], int first, int last, int depth) {
  int size = last - first;
  if (size <= 16) {
    insertionSort(arr, first, last);
    return;
  }

  if (depth == 0) {
    heapSort(arr, first, last + 1);
    return;
  } else {
    int pivot = partition(arr, first, last);
    introSortExecute(arr, first, pivot - 1, depth - 1);
    introSortExecute(arr, pivot + 1, last, depth - 1);
  }
  return;
}

void introSort(int arr[], int size) {
  int depthLimit = (int(2 * std::log(int(size))));
  introSortExecute(arr, 0, size, depthLimit);
}

int randomIndex(int first, int last) {
  std::srand(time(NULL));
  return std::rand() % (last - first) + first;
}

int partition(int arr[], int first, int last) {
  int mid = (first + last) / 2;
  int size = last - first;
  int pivot = medianOfThree(randomIndex(first, last), randomIndex(first, last),
                            randomIndex(first, last));

  // int pivot = mid;
  swap(&arr[pivot], &arr[first]);
  pivot = first;

  while (first < last) {
    if (arr[first] <= arr[last]) {
      swap(&arr[pivot++], &arr[first]);
    }
    ++first;
  }

  swap(&arr[pivot], &arr[last]);

  return pivot;
}

void insertionSort(int arr[], int first, int last) {
  for (int i = first + 1; i <= last; i++) {
    int temp = arr[i];
    int j = i - 1;

    while (j >= first && arr[j] > temp) {
      arr[j + 1] = arr[j];
      j = j - 1;
    }

    arr[j + 1] = temp;
  }
}

void heapify(int *arr, int start, int stop) {
  int n = stop;
  int largest = start;
  int l = 2 * start + 1;
  int r = 2 * start + 2;

  if (l < n && arr[l] > arr[largest])
    largest = l;

  if (r < n && arr[r] > arr[largest])
    largest = r;

  // If largest is not root
  if (largest != start) {
    std::swap(arr[start], arr[largest]);

    heapify(arr, largest, n);
  }
}

void makeHeap(int *arr, int start, int stop) {
  // int startIdx = ((stop - start) / 2) - 1 + start;

  for (int i = stop; i >= start; i--) {
    heapify(arr, i, stop);
  }
}

void heapSort(int a[], int begin, int end) {
  // makeHeap(a, begin, end);
  std::make_heap(&a[begin], &a[end]);
  std::sort_heap(&a[begin], &a[end]);

  // for (int i = end - 1; i >= begin; i--) {
  // std::swap(a[begin], a[i]);
  // makeHeap(a, begin, i);
  // std::make_heap(&a[begin], &a[end]);
  // }
}

int medianOfThree(int a, int b, int c) {
  if (a < b && b < c)
    return b;

  if (a < c && c <= b)
    return c;

  if (b <= a && a < c)
    return a;

  if (b < c && c <= a)
    return c;

  if (c <= a && a < b)
    return a;

  if (c <= b && b <= a)
    return b;

  return b;
}

bool isSorted(int arr[], int begin, int end) {
  for (int i = begin; i < end - 1; i++) {
    if (arr[i] > arr[i + 1]) {
      return false;
    }
  }
  return true;
}

void swap(int *a, int *b) {
  int temp = *a;
  *a = *b;
  *b = temp;
}
