void introSort(int arr[], int size);
int partition(int arr[], int first, int last);
void insertionSort(int arr[], int first, int last);
void heapSort(int arr[], int begin, int end);
bool isSorted(int arr[], int begin, int end);
void printArray(int t[], int size);
void printArray(int t[], int begin, int end);
void swap(int *a, int *b);
