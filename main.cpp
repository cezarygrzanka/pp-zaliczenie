#include "utils/utils.h"

#include <iomanip>
#include <iostream>
#include <random>
#include <string>

void printArray(int t[], int size) {
  std::cout << "--- ARRAY ---\n\n";
  std::cout << std::fixed;

  // I like to use \x1b[XXm to colorize my output.
  for (int i = 0; i < size; i++) {
    std::cout << "\x1b[32m" << i + 1 << "\x1b[0m: " << t[i] << std::endl;
  }

  std::cout << "\n---- END ----\n\n";

  return;
}

void printArray(int t[], int begin, int end) {
  std::cout << "--- ARRAY ---\n\n";
  std::cout << std::fixed;

  // I like to use \x1b[XXm to colorize my output.
  for (int i = begin; i < end; i++) {
    std::cout << "\x1b[32m" << i + 1 << "\x1b[0m: " << t[i] << std::endl;
  }

  std::cout << "\n---- END ----\n\n";

  return;
}

int *createArray(int size) {
  // random seed
  std::random_device rd;

  // Initialize Mersenne Twister pseudo-random number generator
  std::mt19937 gen(rd());

  // Generate pseudo random numbers in range
  std::uniform_real_distribution<> dis(-50, 1000);

  int *arr = new int[size];

  for (int i = 0; i < size; i++) {
    arr[i] = dis(gen);
  }

  return arr;
}

void test(int testSize, int arraySize) {
  int failure = 0;

  int *arrI = createArray(20);
  insertionSort(arrI, 0, 20);
  bool resI = isSorted(arrI, 0, 20);

  int arrHS = 102;

  int *arrH = createArray(arrHS);
  heapSort(arrH, 0, arrHS);
  bool resH = isSorted(arrH, 0, arrHS);

  std::cout << std::endl;
  std::cout << "Inse:   ";
  if (resI)
    std::cout << "\x1b[32m(  OK  )\x1b[0m Works fine";
  else {
    std::cout << "\x1b[32m( ERRO ) FOUND ERROR\x1b[0m";
    return;
  };
  std::cout << std::endl;

  std::cout << "Heap:   ";
  if (resH)
    std::cout << "\x1b[32m(  OK  )\x1b[0m Works fine";
  else {
    std::cout << "\x1b[31m( ERRO ) FOUND ERROR\x1b[0m";
  };
  std::cout << std::endl;

  delete[] arrI;
  delete[] arrH;

  for (int i = 0; i < testSize; i++) {
    int *arr = createArray(arraySize);

    introSort(arr, arraySize);
    // heapSort(arr, 1, arraySize - 10);
    // insertionSort(arr, 1, arraySize - 10);

    bool res = isSorted(arr, 1, arraySize - 10);

    std::cout << "Array " << i;

    if (res) {
      std::cout << " \x1b[32m(  OK  )\x1b[0m has been sorted succesfull"
                << std::endl;
    } else {
      std::cout << " \x1b[31m( ERRO ) wasn't sorted\x1b[0m" << std::endl;
    }

    delete[] arr;
  }

  return;
}

int main() {
  // \0 mean "empy char"
  char userIn = '\0';
  int isAutomatic;

  std::cout << "Welcome in introSort example!" << std::endl;
  std::cout << "Do you want to execute automatic test [Y/n]?: ";

  // Flush is reuired to display previous line in Linux;
  std::flush(std::cout);

  // std::getchar to accept even "empty" line
  userIn = std::getchar();

  isAutomatic = userIn == 'Y' || userIn == 'y' || userIn == '\n';

  if (isAutomatic) {
    int arraysCount = 100;
    int arraysSize = 5000;
    std::string temp;

    std::cout << "How many arrays you want to create? \x1b[32m(default "
              << arraysCount << ")\x1b[0m: ";
    std::getline(std::cin, temp);

    arraysCount = temp != "" && std::stoi(temp) ? std::stoi(temp) : arraysCount;

    std::cout << "Of what size? \x1b[32m(default " << arraysSize
              << ")\x1b[0m: ";
    std::getline(std::cin, temp);

    arraysSize = temp != "" && std::stoi(temp) ? std::stoi(temp) : arraysSize;

    std::cout << "ok... fine... I gonna create " << arraysCount
              << " arrays and execute that test!";

    std::flush(std::cout);
    test(arraysCount, arraysSize);
    return 0;
  }

  int arraySize;

  std::cout << "Soo... how big this array should be?: ";
  std::cin >> arraySize;

  int arr[arraySize];

  for (int i = 0; i < arraySize; i++) {
    std::cout << "arr[" << i << "] = ";
    std::cin >> arr[i];
  }

  std::cout << "There is your array: ";
  printArray(arr, arraySize);
  std::cout << "Now I gonna sort that...";
  introSort(arr, arraySize);
  std::cout << " done" << std::endl << " Let's see is it sorted!: ";
  std::cout << isSorted(arr, 0, arraySize);
  printArray(arr, arraySize);

  return 0;
}
